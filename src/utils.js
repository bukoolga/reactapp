export function convertToCelsius(degrees) {
  return ((degrees-32)*5/9).toPrecision(2);
}
