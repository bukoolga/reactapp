import React, { Component } from 'react';
import WeatherDisplay from './components/weather-display';
import './App.css';
import "bootswatch/dist/darkly/bootstrap.css";

import { BrowserRouter, Route, Link } from 'react-router-dom';
import { Navbar, NavItem, Nav, Grid, Row, Col } from "react-bootstrap";

const PLACES = [
  {name: "minsk", title: "Minsk", zip: "220095"},
  {name: "san-jose", title: "San Jose", zip: "94088"},
  {name: "santa-cruz", title: "Santa Cruz", zip: "95062"},
  {name: "honolulu", title: "Honolulu", zip: "96803"}
];

class App extends Component {
  constructor(props) {
    super(props);
    this.findCityIndex = this.findCityIndex.bind(this);
    console.warn(this.props);
    this.state = {
      activePlace: PLACES.findIndex(this.findCityIndex),
    }
  }
  findCityIndex(element) {
    return element.name === this.props.match.params.city;
  }
  render() {
    const activePlace = this.state.activePlace;
    return (
      <div>
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              React Simple Weather App
            </Navbar.Brand>
          </Navbar.Header>
      </Navbar>
      <Grid>
        <Row>
          <Col md={4} sm={4}>
            <h3>Select a city</h3>
            <Nav
              bsStyle="pills"
              justified
              activeKey={activePlace}
              onSelect={index => {
                  this.setState({ activePlace: index });
                }}
                >
              {PLACES.map((place, index) => (
                <NavItem className="nav-link" key={index} eventKey={index} href={"/"+place.name}>{place.title}</NavItem>
              ))}
              </Nav>
          </Col>
          <Col md={8} sm={8}>
            <WeatherDisplay key={activePlace} zip={PLACES[activePlace].zip} title={PLACES[activePlace].title}  />
          </Col>
        </Row>
      </Grid>
      </div>
    );
  }
}

export default App;
