import React, { Component } from 'react';
import '../App.css';
import "bootswatch/dist/darkly/bootstrap.css";

import { BrowserRouter, Route, Link } from 'react-router-dom';
import { Navbar, NavItem, Nav, Grid, Row, Col } from "react-bootstrap";


class SearchForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <label>
            Enter City Name:
            <input className="form-control" type="text" value={this.state.value} onChange={this.handleChange} />
          </label>
        </div>
        <div className="form-group">
          <input className="btn btn-primary" type="submit" value="Submit" />
        </div>
      </form>
    );
  }
}

export default SearchForm;
