import React, { Component } from 'react';
import SearchForm from './search-form';
import '../App.css';
import "bootswatch/dist/darkly/bootstrap.css";

import { BrowserRouter, Route, Link } from 'react-router-dom';
import { Navbar, NavItem, Nav, Grid, Row, Col } from "react-bootstrap";


class CitySearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ''
    };
  }
  render() {
    return (
      <Row>
        <SearchForm />
      </Row>
    );
  }
}

export default CitySearch;
