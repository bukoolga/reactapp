import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import CitySearch from './components/city-search';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route } from 'react-router-dom';

ReactDOM.render(
  <BrowserRouter>
    <div>
      <Route path="/:city" component={App} />
      <Route exact path="/" component={CitySearch} />
    </div>
  </BrowserRouter>, document.getElementById('root')
);
registerServiceWorker();
